﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using AjaxApp.Models;
namespace LVL2_ASPNet_MVC_04.Controllers
{

    public class HomeController : Controller
    {
        db_customer_imamEntities db = new db_customer_imamEntities;
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult About()
        {
            return View();
        }

        public ActionResult GetData(Tbl_user user)
        {
            user = db.tbl_user.Where(x => x.Id == 1).SingleOrDefault();
            return Json(user, JsonRequestBehavior.AllowGet);
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }

    internal class check
    {
        public string subject { get; set; }
        public string description { get; set; }
    }
}